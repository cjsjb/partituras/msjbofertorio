\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voice 5" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		R1*4  |
%% 5
		r8 e 16 e e 8. f 16 e 8 f 16 e ~ e e 8.  |
		r8 e 16 e e 8. f 16 e 8 f 16 e ~ e c 8.  |
		r8 d 16 d d 8 e 16 d ~ d g, 8. b, 8 d  |
		f 8 e e d 16 e ~ e 4 r  |
		r8 e 16 e e 8. f 16 e 8 f 16 e ~ e e 8.  |
%% 10
		r8 e 16 e e 8. f 16 e 8 f 16 e ~ e c 8.  |
		r8 d 16 d d 8 e 16 d ~ d g, 8. b, 8 d  |
		f 8 e 16 e ~ e 2 r4  |
		R1  |
		\key a \major
		r8 a 16 a a 8. a 16 gis 8 e 16 fis ~ fis gis 8.  |
%% 15
		r8 fis 16 fis fis 8. fis 16 e 8 cis 16 d ~ d e 8.  |
		d 8 cis b, a, 16 b, ~ b, 4 r8 b, 16 cis  |
		d 8 cis b, a, a, 4 b,  |
		r8 a 16 a a 8. a 16 gis 8 e 16 fis ~ fis gis 8.  |
		r8 fis 16 fis fis 8. fis 16 e 8 cis 16 d ~ d e 8.  |
%% 20
		d 8 cis b, a, 16 d ~ d 4 r8 a,  |
		d 8 cis b, a, 16 d ~ d 8 cis b, a, 16 d ~  |
		d 8 cis r b, 16 a, d 8 cis 16 d ~ d e 8.  |
		e 2. r8 a  |
		a 4 e b 8. e 16 ~ e 8 e  |
%% 25
		d' 8. cis' 16 ~ cis' 8 a b 4 r8 a  |
		a 4 e b 8. e 16 ~ e 4  |
		d' 8. cis' 16 ~ cis' 8 a b 4 r8 a  |
		a 4 fis a 8. fis 16 ~ fis 8 fis  |
		\once \override Staff.TimeSignature #'style = #'() \time 2/4
		d' 8. cis' 16 ~ cis' 8 a  |
%% 30
		\time 4/4
		b 2. r8 a  |
		a 4 e b 8. e 16 ~ e 8 e  |
		d' 8. cis' 16 ~ cis' 8 a b 4 r8 a  |
		a 4 e b 8. e 16 ~ e 4  |
		d' 8. cis' 16 ~ cis' 8 a b 4 r8 a  |
%% 35
		a 4 fis a 8. fis 16 ~ fis 4  |
		d' 8. cis' 16 ~ cis' 8 a d' 8. cis' 16 ~ cis' 8 a  |
		a 2. r4  |
		\key a \minor
		R1  |
		R1  |
%% 40
		R1  |
		R1  |
		r8 e 16 e e 8. f 16 e 8 f 16 e ~ e e 8.  |
		r8 e 16 e e 8. f 16 e 8 f 16 e ~ e c 8.  |
		r8 d 16 d d 8 e 16 d ~ d g, 8. b, 8 d  |
%% 45
		f 8 e e d 16 e ~ e 4 r  |
		r8 e 16 e e 8. f 16 e 8 f 16 e ~ e e 16 e 8  |
		r8 e 16 e e 8. f 16 e 8 f 16 e ~ e c 8.  |
		r8 d 16 d d 8 e 16 d ~ d g, 8. b, 8 d  |
		f 8 ( e ) e d 16 e ~ e 2  |
%% 50
		R1  |
		\key a \major
		r8 a 16 a a 8. a 16 gis 8 e 16 fis ~ fis gis 8.  |
		r8 fis 16 fis fis 8. fis 16 e 8 cis 16 d ~ d e 8.  |
		d 8 cis b, a, 16 b, ~ b, 4 r8 b, 16 cis  |
		d 8 cis b, a, a, 4 b,  |
%% 55
		r8 a 16 a a 8. a 16 gis 8 e 16 fis ~ fis gis 8.  |
		r8 fis 16 fis fis 8. fis 16 e 8 cis 16 d ~ d e 8.  |
		d 8 cis b, a, 16 d ~ d 4 r8 a,  |
		d 8 cis b, a, 16 d ~ d 8 cis b, a, 16 d ~  |
		d 8 cis r b, 16 a, d 8 cis 16 d ~ d e 8.  |
%% 60
		e 2. r8 a  |
		a 4 e b 8. e 16 ~ e 8 e  |
		d' 8. cis' 16 ~ cis' 8 a b 4 r8 a  |
		a 4 e b 8. e 16 ~ e 4  |
		d' 8. cis' 16 ~ cis' 8 a b 4 r8 a  |
%% 65
		a 4 fis a 8. fis 16 ~ fis 8 fis  |
		\once \override Staff.TimeSignature #'style = #'() \time 2/4
		d' 8. cis' 16 ~ cis' 8 a  |
		\time 4/4
		b 2. r8 a  |
		a 4 e b 8. e 16 ~ e 8 e  |
		d' 8. cis' 16 ~ cis' 8 a b 4 r8 a  |
%% 70
		a 4 e b 8. e 16 ~ e 4  |
		d' 8. cis' 16 ~ cis' 8 a b 4 r8 a  |
		a 4 fis a 8. fis 16 ~ fis 4  |
		d' 8. cis' 16 ~ cis' 8 a d' 8. cis' 16 ~ cis' 8 a  |
		a 2. r4  |
%% 75
		d' 8. cis' 16 ~ cis' 8 a d' 8. cis' 16 ~ cis' 8 a  |
		a 2. r4  |
		d' 8. cis' 16 ~ cis' 8 a d' 8. cis' 16 ~ cis' 8 a  |
		a 2. r4  |
		R1  |
		\bar "|."
	} % Voice

	\new Lyrics \lyricsto "voice 5" \lyricmode {
		Vi -- "no y" pan hoy "te o" -- fre -- ce __ mos
		en u -- nión con nues -- tras vi __ das
		pa -- ra que Tú se __ as quien trans -- for -- me nues -- tro ser. __

		Pre -- sen -- ta -- mos el es -- fuer __ zo
		del ca -- mi -- no re -- co -- rri __ do
		pa -- ra que Tú guí __ es nues -- tro ca -- mi -- nar.

		Pues la vi -- "da es" u -- una vi __ ña
		y la u -- "va es" nues -- "tro es" -- fuer __ zo,
		tó -- ma -- lo, Se -- ñor,
		y re -- cí -- be -- "lo en" tus ma -- nos.

		Y el pan que pre -- sen -- ta __ mos
		es el fru -- to del tra -- ba __ jo
		de los hom -- bres que, __
		reu -- ni -- dos hoy a -- quí __ te ben -- de -- ci __ mos
		y can -- ta -- mos en __ "tu ho" -- nor.

		¡Ben -- di -- to se -- as, __ ex -- cel -- so __ crea dor!
		¡Ben -- di -- to se -- as, __ oh, Pa -- dre Dios!
		¡Ben -- di -- to se -- as, __ por siem -- pre, __ Se -- ñor!

		¡Ben -- di -- to se -- as, __ ex -- cel -- so __ crea dor!
		¡Ben -- di -- to se -- as, __ oh, Pa -- dre Dios!
		¡Ben -- di -- to se -- as, __ siem -- pre, __ por siem -- pre, __ Se -- ñor!

		Nues -- tra fe, nues -- "tra es" -- pe -- ran __ za,
		han que -- da -- do ya dis -- pues __ tas
		en es -- tas o -- fren __ das "que hoy" po -- ne -- mos en "tu al" -- tar. __

		Que tu a -- mor hoy las re __ci -- ba
		y tus ma -- nos las trans -- for __ men
		en el cuer -- "po y" san __ gre de tu hi __ jo Je -- sús. __

		Pues la vi -- "da es" u -- una vi __ ña
		y la u -- "va es" nues -- "tro es" -- fuer __ zo,
		tó -- ma -- lo, Se -- ñor,
		y re -- cí -- be -- "lo en" tus ma -- nos.

		Y el pan que pre -- sen -- ta __ mos
		es el fru -- to del tra -- ba __ jo
		de los hom -- bres que, __
		reu -- ni -- dos hoy a -- quí __ te ben -- de -- ci __ mos
		y can -- ta -- mos en __ "tu ho" -- nor.

		¡Ben -- di -- to se -- as, __ ex -- cel -- so __ crea dor!
		¡Ben -- di -- to se -- as, __ oh, Pa -- dre Dios!
		¡Ben -- di -- to se -- as, __ por siem -- pre, __ Se -- ñor!

		¡Ben -- di -- to se -- as, __ ex -- cel -- so __ crea dor!
		¡Ben -- di -- to se -- as, __ oh, Pa -- dre Dios!
		¡Ben -- di -- to se -- as, __ siem -- pre, __ por siem -- pre, __ Se -- ñor!
		¡Siem -- pre, __ por siem -- pre, __ Se -- ñor!
		¡Siem -- pre, __ por siem -- pre, __ Se -- ñor!

	} % Lyrics 1
>> % Staff ends
