	\context ChordNames \chords { 
		% intro
		a2:m a2:m/g a2:m/fis a2:m/f
		a2:m a2:m/g a2:m/fis a2:m/f
		% estrofa 1
		a2:m a2:m/g a2:m/fis a2:m/f
		d2:m g2:7 c2 e2
		a2:m a2:m/g a2:m/fis a2:m/f
		d2:m g2:7 c1 e1 \break
		% puente
		a2 e2 fis2:m cis2:m b1:m d2 e2
		a2 e2 fis2:m cis2:m d1 b1:m g1 e1
		% coro
		a2 e2:/a d2:/a e2:/a a2 e2:/a d2:/a e2:/a
		fis2:m d2 b2:m e1
		a2 e2 d2 e2 a2 e2 d2 e2
		fis2:m d2 b2:m e2 a1 \break
		% interludio
		a2:m a2:m/g a2:m/fis a2:m/f
		a2:m a2:m/g a2:m/fis a2:m/f
		% estrofa 2
		a2:m a2:m/g a2:m/fis a2:m/f
		d2:m g2:7 c2 e2
		a2:m a2:m/g a2:m/fis a2:m/f
		d2:m g2:7 c1 e1 \break
		% puente
		a2 e2 fis2:m cis2:m b1:m d2 e2
		a2 e2 fis2:m cis2:m d1 b1:m g1 e1
		% coro
		a2 e2:/a d2:/a e2:/a a2 e2:/a d2:/a e2:/a
		fis2:m d2 b2:m e1
		a2 e2 d2 e2 a2 e2 d2 e2
		fis2:m d2 b2:m e2 a1
		b2:m e2 a1
		b2:m e2 a1 a1
	}
